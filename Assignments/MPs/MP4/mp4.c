/* A semiprime number is a product of two prime numbers.  This implies that it has
 * only 4 factors: 1, P1, P2, and itself.  P1 and P2 may be identical, that is a
 * semiprime may be the square of a prime number.
 *
 * This program prints a list of the semiprimes on an interval supplied by the user.
 * Inputs must be positive integers and the interval should be positively oriented,
 * that is for interval [a,b], a<=b.  Also note that if a or b are semiprime, they
 * are also included.
 */

#include <stdlib.h>
#include <stdio.h>

int is_prime(int n);
int print_semiprimes(int a, int b);

int main() {
    int a, b;
    printf("Input two numbers: ");
    scanf("%d %d", &a, &b);
    if (a <= 0 || b <= 0) {
        printf("Inputs should be positive integers\n");
        return 1;
    }

    if (a > b) {
        printf("The first number should be smaller than or equal to the second number\n");
        return 1;
    }

    print_semiprimes(a, b);

    return 0;
}


/* This function checks if the number is prime or not.
 * Input   : a number
 * Return  : 0 if the number is not prime, else 1
 */
int is_prime(int n) {
    // if n is 1, 0, or negative then it is not prime
    if (n <= 1) {
        return 0;
    }
    // otherwise, if n is 2 or 3, then it is prime
    else if (n <= 3) {
        return 1;
    }
    // any multiple of 2 or 3 is obviously not prime
    else if (n % 2 == 0 || n % 3 == 0) {
        return 0;
    }

    // This next bit uses the observation that all primes can be
    // written as (6k±1).  This is because all integers can be
    // written as (6k+i) for i=-1,0,1,2,3,4.  (6k+0), (6k+2),
    // and (6k+4) are all even, so we can exclude them. Likewise,
    // (6k+3) is divisible by 3, so it is not prime. Also, 6k is
    // clearly not prime, so all primes must be of the form (6k±1)

    // If we rename 6k-1 as i, then 6k+1 is simply i+2
    // Starting with k=1, we have i=5, and i+2=7, which are the
    // next two primes after 2 and 3.  Incrementing by 6 will yield
    // the next two.  Rinse and repeat.
    int i = 5;
    while (i * i <= n) {
        // Note: we stop when i > sqrt(n) because any factors above
        // sqrt(n) would have a co-factor less than sqrt(n), so we
        // would have found it already.
        if (n % i == 0 || n % (i+2) == 0) {
            return 0;
        }
        i += 6;
    }
    return 1;
}


/* This function prints all semiprimes in [a,b] (including a, b).
 * Input   : a, b (a should be smaller than or equal to b)
 * Return  : 0 if there is no semiprime in [a,b], else 1
 */
int print_semiprimes(int a, int b) {
    int has_semiprimes = 0;     // flag to determine return value
    int n;
    for (n = a; n <= b; n ++) {
        int k;
        for (k = 2; k < n; k ++) {
            // for n to be semiprime, k must be a factor of n,
            // k must be prime, and n/k (the co-factor) must be prime.
            if ((n % k == 0) && is_prime(k) && is_prime(n/k)) {
                // if we have already found a semiprime, print a space
                // before the next one.  Otherwise this is the first one
                if (has_semiprimes == 1) {
                    printf(" ");
                }
                printf("%d", n);
                has_semiprimes = 1; // set the flag and break from the
                break;              // inner for-loop to check the next n
            }
        }
    }
    // print a newline character after the list of semiprimes
    printf("\n");
    return has_semiprimes;
}
