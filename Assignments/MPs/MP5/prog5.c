/* Eric Roch - MP5
 * This is a simple guessing game where a random 4 digit number is generated and
 * the user must guess the number in six tries or less. After each attempt, the
 * guess is compared to the solution and the user is told how many digits they
 * guessed correctly in the correct spot, and how many they guessed correctly in
 * the incorrect spot.
 */


/*
 * The functions that you must write are defined in the header file.
 * Blank function prototypes with explanatory headers are provided
 * in this file to help you get started.
 */
#include <stdio.h>
#include <stdlib.h>

#include "prog5.h"


/*
 * You will need to keep track of the solution code using file scope
 * variables as well as the guess number.
 */

static int guess_number;
static int solution1;
static int solution2;
static int solution3;
static int solution4;

/*
 * in_range --  This helper function is used by make_guess to determine if
 *              a number is in a specified range.
 * INPUTS: value -- the number to verify is in the range
 *         min -- the lower bound of the range (inclusive)
 *         max -- the upper bound of the range (inclusive)
 * RETURN VALUE: 1 if value is between min and max, inclusive
 *               0 otherwise
 */
int
in_range(int value, int min, int max) {
    return (min <= value && value <= max);
}

/*
 * set_seed -- This function sets the seed value for pseudorandom
 * number generation based on the number the user types.
 * The input entered by the user is already stored in the string seed_str by the code in main.c.
 * This function should use the function sscanf to find the integer seed value from the
 * string seed_str, then initialize the random number generator by calling srand with the integer
 * seed value. To be valid, exactly one integer must be entered by the user, anything else is invalid.
 * INPUTS: seed_str -- a string (array of characters) entered by the user, containing the random seed
 * OUTPUTS: none
 * RETURN VALUE: 0 if the given string is invalid (string contains anything
 *               other than a single integer), or 1 if string is valid (contains one integer)
 * SIDE EFFECTS: initializes pseudo-random number generation using the function srand. Prints "set_seed: invalid seed\n"
 *               if string is invalid. Prints nothing if it is valid.
 */
int
set_seed (const char seed_str[])
{
    int seed;       // integer to hold seed value
    char post[2];   // dummy array to hold extra input (if any)

    int scan_value = sscanf(seed_str, "%d%1s", &seed, post);

    if (scan_value == 1) {
        srand(seed);
        return 1;
    }

    // implied else --> scan_value != 1
    printf("set_seed: invalid seed\n");
    return 0;
}


/*
 * start_game -- This function is called by main.c after set_seed but before the user makes guesses.
 *               This function creates the four solution numbers using the approach
 *               described in the wiki specification (using rand())
 *               The four solution numbers should be stored in the static variables defined above.
 *               The values at the pointers should also be set to the solution numbers.
 *               The guess_number should be initialized to 1 (to indicate the first guess)
 * INPUTS: none
 * OUTPUTS: *one -- the first solution value (between 1 and 6)
 *          *two -- the second solution value (between 1 and 6)
 *          *three -- the third solution value (between 1 and 6)
 *          *four -- the fourth solution value (between 1 and 6)
 * RETURN VALUE: none
 * SIDE EFFECTS: records the solution in the static solution variables for use by make_guess, set guess_number
 */
void
start_game (int* one, int* two, int* three, int* four)
{
    *one   = solution1 = rand() % 6 + 1;
    *two   = solution2 = rand() % 6 + 1;
    *three = solution3 = rand() % 6 + 1;
    *four  = solution4 = rand() % 6 + 1;

    guess_number = 1;

    // print solution for testing on other systems
    // printf("Solution: %d %d %d %d\n", solution1, solution2, solution3, solution4);
}

/*
 * make_guess -- This function is called by main.c after the user types in a guess.
 *               The guess is stored in the string guess_str.
 *               The function must calculate the number of perfect and misplaced matches
 *               for a guess, given the solution recorded earlier by start_game
 *               The guess must be valid (contain only 4 integers, within the range 1-6). If it is valid
 *               the number of correct and incorrect matches should be printed, using the following format
 *               "With guess %d, you got %d perfect matches and %d misplaced matches.\n"
 *               If valid, the guess_number should be incremented.
 *               If invalid, the error message "make_guess: invalid guess\n" should be printed and 0 returned.
 *               For an invalid guess, the guess_number is not incremented.
 * INPUTS: guess_str -- a string consisting of the guess typed by the user
 * OUTPUTS: the following are only valid if the function returns 1 (A valid guess)
 *          *one -- the first guess value (between 1 and 6)
 *          *two -- the second guess value (between 1 and 6)
 *          *three -- the third guess value (between 1 and 6)
 *          *four -- the fourth color value (between 1 and 6)
 * RETURN VALUE: 1 if the guess string is valid (the guess contains exactly four
 *               numbers between 1 and 6), or 0 if it is invalid
 * SIDE EFFECTS: prints (using printf) the number of matches found and increments guess_number(valid guess)
 *               or an error message (invalid guess)
 *               (NOTE: the output format MUST MATCH EXACTLY, check the wiki writeup)
 */
int
make_guess (const char guess_str[], int* one, int* two, int* three, int* four)
{
//  One thing you will need to read four integers from the string guess_str, using a process
//  similar to set_seed
//  The statement, given char post[2]; and four integers w,x,y,z,
//  sscanf (guess_str, "%d%d%d%d%1s", &w, &x, &y, &z, post)
//  will read four integers from guess_str into the integers and read anything else present into post
//  The return value of sscanf indicates the number of items sucessfully read from the string.
//  You should check that exactly four integers were sucessfully read.
//  You should then check if the 4 integers are between 1-6. If so, it is a valid guess
//  Otherwise, it is invalid.
//  Feel free to use this sscanf statement, delete these comments, and modify the return statement as needed
    int w, x, y, z;
    char post[2];
    int valid_guess = 1;

    int scan_value = sscanf(guess_str, "%d%d%d%d%1s", &w, &x, &y, &z, post);

    // not valid if there were not exactly four inputs
    if (scan_value != 4) {
        valid_guess = 0;
    }
    // not valid if any of the inputs are not between 1 and 6
    if (!in_range(w, 1, 6) || !in_range(x, 1, 6) ||
        !in_range(y, 1, 6) || !in_range(z, 1, 6)) {
        valid_guess = 0;
    }

    if (!valid_guess) {
        printf("make_guess: invalid guess\n");
        return 0;
    }

    // The guess was valid, so lets store the output values and calculate exact/misplaced matches
    *one = w;
    *two = x;
    *three = y;
    *four = z;

    int perfect = 0;
    int misplaced = 0;
    int solution[] = {solution1, solution2, solution3, solution4};
    int guess[] = {w, x, y, z};
    int solution_paired[4];     // array to track solution numbers which are paired already
    int guess_paired[4];        // array to track guess numbers which are paired already

    // Step through the guess and solution once, looking for perfect matches
    for (int i = 0; i < 4; i ++) {
        if (solution[i] == guess[i]) {
            solution_paired[i] = 1;
            guess_paired[i] = 1;
            perfect++;
        }
        else {
            solution_paired[i] = 0;
            guess_paired[i] = 0;
        }
    }

    // Step through again, this time comparing each guess value to the remaining solution values
    // i - index of guess[], j - index of solution[]
    for (int i = 0; i < 4; i ++) {
        // if this guess value is already paired, skip it
        if (guess_paired[i] == 1) {
            continue;
        }
        // otherwise, check the unpaired solution values for a match
        for (int j = 0; j < 4; j ++) {
            // if i == j, we compared these last time, so we can skip it here
            // also, if this solution value is already paired, skip it
            if (i == j || solution_paired[j] == 1) {
                continue;
            }

            // if the guess matches the solution, set the paired flags,
            // increment misplaced counter, and break to check the next guess value
            if (guess[i] == solution[j]) {
                guess_paired[i] = 1;
                solution_paired[j] = 1;
                misplaced++;
                break;
            }
        }
    }

    printf("With guess %d, you got %d perfect matches and %d misplaced matches.\n", guess_number, perfect, misplaced);
    guess_number++;
    return 1;
}
