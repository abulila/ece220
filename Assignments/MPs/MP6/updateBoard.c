#include <stdlib.h>

/* Eric Roch - MP6
 * This file includes functions to generate the next iteration of a board for
 * Conway's Game of Life.  updateBoard() will generate a new board based on the number
 * of live neighbors each cell has (using the countLiveNeighbor() method) and return
 * the new board.  aliveStable() indicates when the game has reached a steady state
 * and no longer needs to be updated.  This implimentation follows the B3/S23 ruleset.
 */

int xy2index(int x, int y, int width);


/*
 * countLiveNeighbor
 * Inputs:
 * board: 1-D array of the current game board. 1 represents a live cell.
 * 0 represents a dead cell
 * boardRowSize: the number of rows on the game board.
 * boardColSize: the number of cols on the game board.
 * row: the row of the cell that needs to count alive neighbors.
 * col: the col of the cell that needs to count alive neighbors.
 * Output:
 * return the number of alive neighbors. There are at most eight neighbors.
 * Pay attention for the edge and corner cells, they have less neighbors.
 */
int
countLiveNeighbor(int* board, int boardRowSize, int boardColSize, int row, int col)
{
    int count = 0;
    int x = col, y = row;       // use x and y for easier visualization
    int width = boardColSize;   // use width to shorten helper calls

    // if there are cells to the left...
    if (x - 1 >= 0) {
        count += board[xy2index(x - 1, y, width)];
        // if there are cells above...
        if (y - 1 >= 0) {
            count += board[xy2index(x - 1, y - 1, width)];
        }
        // if there are cells below...
        if (y + 1 < boardRowSize) {
            count += board[xy2index(x - 1, y + 1, width)];
        }
    }

    // if there are cells to the right...
    if (x + 1 < width) {
        count += board[xy2index(x + 1, y, width)];
        // if there are cells above...
        if (y - 1 >= 0) {
            count += board[xy2index(x + 1, y - 1, width)];
        }
        // if there are cells below...
        if (y + 1 < boardRowSize) {
            count += board[xy2index(x + 1, y + 1, width)];
        }
    }

    // now check above and below...
    if (y - 1 >= 0) {
        count += board[xy2index(x, y - 1, width)];
    }
    if (y + 1 < boardRowSize) {
        count += board[xy2index(x, y + 1, width)];
    }

    return count;
}
/*
 * Update the game board to the next step.
 * Input:
 * board: 1-D array of the current game board. 1 represents a live cell.
 * 0 represents a dead cell
 * boardRowSize: the number of rows on the game board.
 * boardColSize: the number of cols on the game board.
 * Output: board is updated with new values for next step.
 */
void
updateBoard(int* board, int boardRowSize, int boardColSize)
{
    int next_gen[boardRowSize*boardColSize];    // create a temporary array to store the next generation

    int x,y;
    for (y = 0; y < boardRowSize; y ++) {
        for (x = 0; x < boardColSize; x ++) {
            // calculate the 1-D index now, so we can use it later
            int index = xy2index(x, y, boardColSize);

            // update the next_gen array based on the number of live neighbors around this cell.
            // We can shortcut this calculation by noticing that a cell with 3 neighbors will
            // either be born, or stay alive, so 3 will always result in an alive cell.
            // Likewise, less than 2 and more than 3 will always result in death.  Exactly 2
            // will keep live cells alive and dead cells dead.
            int neighbors = countLiveNeighbor(board, boardRowSize, boardColSize, y, x);
            if (neighbors == 3) {
                next_gen[index] = 1;
            }
            else if (neighbors < 2 || neighbors > 3) {
                next_gen[index] = 0;
            }
            else {
                next_gen[index] = board[index];
            }
        }
    }

    // now we have to copy the values from next_gen back into board
    int i = 0;
    while (i < boardRowSize * boardColSize) {
        board[i] = next_gen[i];
        i ++;
    }
}

/*
 * aliveStable
 * Checks if the alive cells stay the same for next step
 * board: 1-D array of the current game board. 1 represents a live cell.
 * 0 represents a dead cell
 * boardRowSize: the number of rows on the game board.
 * boardColSize: the number of cols on the game board.
 * Output: return 1 if the alive cells for next step is exactly the same with
 * current step or there is no alive cells at all.
 * return 0 if the alive cells change for the next step.
 */
int aliveStable(int* board, int boardRowSize, int boardColSize){
    int x,y;
    for (y = 0; y < boardRowSize; y ++) {
        for (x = 0; x < boardColSize; x ++) {
            // calculate the 1-D index now, so we can use it later
            int index = xy2index(x, y, boardColSize);

            int neighbors = countLiveNeighbor(board, boardRowSize, boardColSize, y, x);

            // if exactly 2 neighbors, the cell does not change
            if (neighbors == 2) {
                continue;
            }
            // live cells with 3 neighbors do not change
            else if (board[index] == 1 && neighbors == 3) {
                continue;
            }
            // dead cells with 0,1, or 3+ neighbors also do not change
            else if (board[index] == 0  && (neighbors < 2 || neighbors > 3)) {
                continue;
            }
            else {
                return 0;
            }
        }
    }
    return 1;
}

int xy2index(int x, int y, int width) {
    return x + y*width;
}
