#include <stdlib.h>
#include "dice.h"

//Function for generating three d6 rolls
void roll_three(int* one, int* two, int* three){
    // genreate 3 random numbers between 1 and 6
    *one = rand() % 6 + 1;
    *two = rand() % 6 + 1;
    *three = rand() % 6 + 1;

}
