.ORIG x3000
; Write code to read in characters and echo them
; till a newline character is entered.
GET_CHAR
        GETC
        OUT

        LD R6, NEW_LINE
        NOT R6, R6
        ADD R6, R6, #1
        ADD R6, R6, R0
        BRz CHECK_STACK

        LD R6, SPACE
        NOT R6, R6
        ADD R6, R6, #1
        ADD R6, R6, R0
        BRz GET_CHAR

        JSR IS_BALANCED
        ADD R5, R5, #0
        BRzp GET_CHAR
        ADD R1, R5, #0
        BR GET_CHAR

CHECK_STACK
        LD R6, STACK_TOP
        LD R7, STACK_START
        NOT R7, R7
        ADD R7, R7, #1
        ADD R6, R6, R7
        BRz BALANCED
        BR UNBALANCED
BALANCED
        ADD R1, R1, #0
        BRn UNBALANCED
        AND R5, R5, #0
        ADD R5, R5, #1
        LEA R0, BALANCED_STR
        PUTS
        BR DONE
UNBALANCED
        AND R5, R5, #0
        ADD R5, R5, #-1
        LEA R0, UNBALANCED_STR
        PUTS
DONE
        HALT

SPACE           .FILL x0020
NEW_LINE        .FILL x000A
CHAR_RETURN     .FILL x000D

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;if ( push onto stack if ) pop from stack and check if popped value is (
;input - R0 holds the input
;output - R5 set to -1 if unbalanced. else not modified.
IS_BALANCED
        ST R6, BAL_SaveR6
        ST R7, BAL_SaveR7

        LD R6, NEG_OPEN
        ADD R6, R6, R0
        BRz OPEN
        ADD R6, R6, #-1
        BRz CLOSE
        BR RETURN

OPEN
        JSR PUSH
        BR RETURN
CLOSE
        JSR POP
        LD R6, NEG_OPEN
        ADD R6, R6, R0
        BRz SUCCESS
        BR FAIL

SUCCESS
        AND R5, R5, #0
        ADD R5, R5, #1
        BR RETURN
FAIL
        AND R5, R5, #0
        ADD R5, R5, #-1
        BR RETURN
RETURN
        LD R6, BAL_SaveR6
        LD R7, BAL_SaveR7
        RET

NEG_OPEN        .FILL xFFD8
BAL_SaveR6      .BLKW #1
BAL_SaveR7      .BLKW #1

;IN:R0, OUT:R5 (0-success, 1-fail/overflow)
;R3: STACK_END R4: STACK_TOP
;
PUSH
        ST R3, PUSH_SaveR3      ;save R3
        ST R4, PUSH_SaveR4      ;save R4
        AND R5, R5, #0          ;
        LD R3, STACK_END        ;
        LD R4, STACk_TOP        ;
        ADD R3, R3, #-1         ;
        NOT R3, R3              ;
        ADD R3, R3, #1          ;
        ADD R3, R3, R4          ;
        BRz OVERFLOW            ;stack is full
        STR R0, R4, #0          ;no overflow, store value in the stack
        ADD R4, R4, #-1         ;move top of the stack
        ST R4, STACK_TOP        ;store top of stack pointer
        BRnzp DONE_PUSH         ;
OVERFLOW
        ADD R5, R5, #1          ;
DONE_PUSH
        LD R3, PUSH_SaveR3      ;
        LD R4, PUSH_SaveR4      ;
        RET


PUSH_SaveR3     .BLKW #1        ;
PUSH_SaveR4     .BLKW #1        ;


;OUT: R0, OUT R5 (0-success, 1-fail/underflow)
;R3 STACK_START R4 STACK_TOP
;
POP
        ST R3, POP_SaveR3       ;save R3
        ST R4, POP_SaveR4       ;save R3
        AND R5, R5, #0          ;clear R5
        LD R3, STACK_START      ;
        LD R4, STACK_TOP        ;
        NOT R3, R3              ;
        ADD R3, R3, #1          ;
        ADD R3, R3, R4          ;
        BRz UNDERFLOW           ;
        ADD R4, R4, #1          ;
        LDR R0, R4, #0          ;
        ST R4, STACK_TOP        ;
        BRnzp DONE_POP          ;
UNDERFLOW
        ADD R5, R5, #1          ;
DONE_POP
        LD R3, POP_SaveR3       ;
        LD R4, POP_SaveR4       ;
        RET


POP_SaveR3      .BLKW #1        ;
POP_SaveR4      .BLKW #1        ;


BALANCED_STR    .STRINGZ "The input string is balanced"
UNBALANCED_STR  .STRINGZ "The input string is not balanced"
STACK_END       .FILL x3FF0     ;
STACK_START     .FILL x4000     ;
STACK_TOP       .FILL x4000     ;

.END
