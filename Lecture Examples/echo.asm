.ORIG x3000

; Check for new character from keyboard
CHECK   LDI   R0,KBSR
        BRzp  CHECK

        LDI   R6,KBDR

; Wait until the display is ready
READY   LDI   R0,DSR
        BRzp  READY

        STI   R6,DDR

        HALT

KBSR    .FILL xFE00
KBDR    .FILL xFE02
DSR     .FILL xFE04
DDR     .FILL xFE06
.END
