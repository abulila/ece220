;Load ASCII art character, stored at memory address x5000 (IMAGEADDR)
;The first two numbers are the width (number of columns) and the height (number of rows) in the ASCII art image
;The memory addresses starting at x5002 are ASCII characters. The first N characters are the first row of the image, the second N characters are the second row of the image, etc.
;each row should end with a newline character

.ORIG x3000
;YOUR CODE GOES HERE
    LD R6, IMAGEADDR
    LDR R1, R6, #0      ; R1 <- N (columns)
    BRnz DONE
    LDR R2, R6, #1      ; R2 <- M (rows)
    BRnz DONE

    ADD R6, R6, #2      ; point R6 to the first ASCII value
ROW_LOOP
    ADD R3, R1, #0      ; character counter
CHAR_LOOP
    LDR R0, R6, #0      ; load ASCII to R0
    OUT                 ; print
    ADD R6, R6, #1      ; move pointer to next character
    ADD R3, R3, #-1     ; decrease char counter
    BRp CHAR_LOOP       ; loop if positive
    LD R0, NEWLINE      ; otherwise print newline
    OUT
    ADD R2, R2, #-1     ; and decrease row counter
    BRp ROW_LOOP

DONE
    HALT

NEWLINE   .FILL x000A
IMAGEADDR .FILL x5000 ; address of image
.END
