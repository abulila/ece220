.ORIG x3000

LD R1, INPUT
JSR FIBON
ST R6, RESULT
HALT

;your code goes here
FIBON
    AND R3, R3, #0      ; R3: F_low
    ADD R4, R4, #0      ; R4: F_high
    ADD R3, R3, #1
    ADD R4, R4, #1

    ADD R0, R1, #-2
    BRnz DONE
LOOP
    ADD R1, R3, R4      ; F_sum = F_low + F_high
    BRn OVERFLOW
    ADD R3, R4, #0      ; F_low = F_high
    ADD R4, R1, #0      ; F_high = F_sum
    ADD R0, R0, #-1
    BRp LOOP
    BR DONE

OVERFLOW
    AND R4, R4, #0
    ADD R4, R4, #-1
DONE
    ADD R6, R4, #0      ; R6 <- F_high
    RET






INPUT 	.FILL x0005
RESULT	.BLKW #1
.END
